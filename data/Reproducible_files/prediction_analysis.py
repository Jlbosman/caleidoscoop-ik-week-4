#!/usr/bin/python3
# File name: exercise1.py
# User has to input 2 files, first with prediction numbers
# second file, sentences that match the predition numbers
# The script gives as output all files that the system  gives a wrong label
# Author: Larisa Bulbaai
# Date: 27-05-2019

import sys
import nltk
from nltk.tokenize import sent_tokenize, word_tokenize
from nltk.stem import WordNetLemmatizer
from nltk import pos_tag, word_tokenize
from nltk.corpus import wordnet as wn


def low_rate(text, text2):

    f = open("Low_prediction", "w+")

    sentences = (text.split("\n"))
    list_predictions = []
    output_list = []
    list_labels = []

    sentences2 = (text2.split("\n"))

    for i in sentences:
        new = i.split(" ")
        if len(new) > 1:
            list_predictions.append(new[1])
            list_labels.append(new[0])

    count = 0

    for i in sentences2[:-1]:
        splitted = i.split("\t")
        correct_label = splitted[0]
        count += 1
        if correct_label != list_labels[count-1]:
            i = "CORRECT LABEL:" + i + "\n" + " PREDICTED LABEL:"+list_labels[count-1] + " RATE:" + list_predictions[count-1]

            output_list.append(i)

    for i in output_list:
        f.write(i + '\n')

    print("All sentences and their prediction,"
          "are printed to file 'Low_prediction'")


def main(argv):

    # Lets user know if files weren't inserted.
    if len(argv) < 2:
        print("Usage: {} <text file>\n\nThere has to be atleast 2 arguments,"
              "\ninsert two files you want to analyse. First file"
              " is file with prediction numbers, second file is file,"
              " with the matching sentences."
              .format(argv[0]), file=sys.stderr)
        exit(-1)

    file1 = sys.argv[1]
    file2 = sys.argv[2]

    with open(file1) as f1:
        file1 = f1.read()

    with open(file2) as f2:
        file2 = f2.read()

    low_rate(file1, file2)


if __name__ == "__main__":
    main(sys.argv)